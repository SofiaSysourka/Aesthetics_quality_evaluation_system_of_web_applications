from __future__ import division
from math import factorial, pow, gamma, sqrt
import re
import json

print ('hello metrics!')

def density(url, path):
    #Reading data
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)
        
    DM = []
    print("\nnumOfLayers = "+str(data["scrapingResults"]["numOfLayers"]))
    print("density")
    for i in range(data["scrapingResults"]["numOfLayers"]):     
        DM.append(1 - sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"])/data["scrapingResults"]["frameArea"])
        print(DM[i])
    return DM     


    
def equilibrium(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)
        
        
    EM = [] 
    print("\n")
    print("equilibrium ")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"]
        bframe = data["scrapingResults"]["frameWidth"]
        hframe = data["scrapingResults"]["frameHeight"]
        sumx = 0
        sumy = 0
    
        # ObjectArea, ObjCenterX, ObjCenterY keys have n number of values (n = number of elements)
        for area,xi,yi in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterX"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterY"]):
            sumx = sumx + area*(xi-data["scrapingResults"]["frameCenterX"])
            sumy = sumy + area*(yi-data["scrapingResults"]["frameCenterY"])
    
        EMver = 2*sumx/(n*bframe*sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"]))
        EMhor = 2*sumy/(n*hframe*sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"]))    
     
        EM.append(1 - (abs(EMver)+abs(EMhor))/2)
        print(EM[i])
        
    return EM     
        
def balance(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    BM = []   
    print("\n")
    print("balance")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        wL = 0
        wR = 0
        wT = 0
        wB = 0
    
        for area,dist in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemTopArea"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemDistTop"]):
            wT = wT + area*dist
        for area,dist in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemBottomArea"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemDistBottom"]):
            wB = wB + area*dist
        for area,dist in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemRightArea"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemDistRight"]):
            wR = wR + area*dist
        for area,dist in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemLeftArea"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ElemDistLeft"]):
            wL = wL + area*dist
        
        if wL==0 and wR==0:
            BMver = 0
        else:
            BMver = (wL-wR)/max(wL,wR)

        if wB==0 and wT==0:
            BMhor = 0
        else:
            BMhor = (wT-wB)/max(wT,wB)
        
        BM.append(1 - (abs(BMver)+abs(BMhor))/2)
            
        print(BM[i])
        
    return BM  
  

def regularity(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    RM = []   
    print("\n")
    print("regularity")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        nvap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["StartingVap"]
        nhap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["StartingHap"]
        nspacing = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["RowColSpacing"]
        
        RMalignment = 1 - (nvap + nhap)/(2*n)
        if n==1:
            RMspacing = 1
        else:
            RMspacing = 1 - (nspacing-1)/(2*(n-1))
        
        RM.append((abs(RMalignment)+abs(RMspacing))/2)
            
        print(RM[i])
        
    return RM  



def homogeneity(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    H = []
    print("\n")
    print("homogeneity")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        UL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UL"]["numOfElems"]
        UR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UR"]["numOfElems"]
        LL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LL"]["numOfElems"]
        LR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LR"]["numOfElems"]
        
        # if n is too large, there will be an overflow and we can't calculate the metric
        # generally, as n increases homogeneity decreases
        if n>150:
            H.append('NaN')
            print('unable to calculate')
            continue  
        
        W = factorial(n)/(factorial(UL)*factorial(UR)*factorial(LL)*factorial(LR))            
        Wmax = factorial(n)/pow(gamma((n/4)+1),4)

        H.append(W/Wmax)
            
        print(H[i])
        
    return H 


def simplicity(url,path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    SMM = []    
    print("\n")
    print("simplicity")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        nvap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["StartingVap"]
        nhap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["StartingHap"]

        SMM.append(3/(nvap + nhap + n))
            
        print(SMM[i])
        
    return SMM 


def alignment(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    AM = [];     
    print("\n")
    print("alignment")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        nvap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["allVap"]
        nhap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["allHap"]

        if n==1:
            AM.append(1)
        else:    
            AM.append((4*n - (nhap*nvap/n))/(4*n - 4))
                        
        print(AM[i])
        
    return AM 


def grouping(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    GM = []    
    print("\n")
    print("grouping")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        nvap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["allVap"]
        nhap = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["allHap"]
        alayout = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["layoutArea"]
        aframe = data["scrapingResults"]["frameArea"]

        if n==1:
            AL = 1
        else:
            AL = min(1, (4*n - (nhap*nvap/n))/(4*n - 4))
        
        sumOfAreas = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"])   

        if sumOfAreas==aframe:
            UMspace = 0
        else:
            UMspace = 1 - (alayout-sumOfAreas)/(aframe-sumOfAreas)   
            
        GM.append(0.85*UMspace + 0.15*AL)
            
        print(GM[i])
        
    return GM 


def proportion(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    PM = []    
    print("\n")
    print("proportion")
    proportions = [1, 1/1.414, 1/1.618, 1/1.732, 0.5]
    
    for i in range(data["scrapingResults"]["numOfLayers"]): 
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        Hlayout = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["layoutHeight"]
        Blayout = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["layoutWidth"]
        rlayout = Hlayout/Blayout
        objSum = 0
        
        for h,b in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"]):
            r = h/b
            if r>1:
                p = 1/r
            else:
                p = r
            
            if rlayout>1:
                playout = 1/rlayout
            else:
                playout = rlayout 
                
            objSum = objSum + 1 - min([abs(x - p) for x in proportions])/0.5 
     
        PMobject = (1/n)*objSum
        PMlayout = 1 - min([abs(x - playout) for x in proportions])/0.5
        
        PM.append((abs(PMobject)+abs(PMlayout))/2)
            
        print(PM[i])
        
    return PM


def cohesion(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    CM = []    
    print("\n")
    print("cohesion")
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        n = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["NumOfElements"] 
        Hlayout = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["layoutHeight"]
        Blayout = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["layoutWidth"]
        Hframe = data["scrapingResults"]["frameHeight"]
        Bframe = data["scrapingResults"]["frameWidth"]
        
        sumf = 0
        
        for h,b in zip(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"],data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"]):

            t = (h/b)/(Hlayout/Blayout)
            if t>1:
                f = 1/t
            else:
                f = t
                
            sumf = sumf + f    
            
        tfl = (Hlayout/Blayout)/(Hframe/Bframe)
        if tfl>1:
            CMfl = 1/tfl
        else:
            CMfl = tfl
        CMlo = sumf/n
        
        CM.append((abs(CMfl)+abs(CMlo))/2)
            
        print(CM[i])
        
    return CM


def symmetry(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    SYM = []    
    print("\n")
    print("symmetry")
    frameCntX = data["scrapingResults"]["frameCenterX"]
    frameCntY = data["scrapingResults"]["frameCenterY"]
        
    for i in range(data["scrapingResults"]["numOfLayers"]):  
        objectsCntX = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterX"]
        objectsCntY = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterY"]
        elemUL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UL"]["elemID"]
        elemUR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UR"]["elemID"]
        elemLL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LL"]["elemID"]
        elemLR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LR"]["elemID"]
        
        xUL = sum([abs(objectsCntX[j] - frameCntX) for j in elemUL])
        xUR = sum([abs(objectsCntX[j] - frameCntX) for j in elemUR])
        xLL = sum([abs(objectsCntX[j] - frameCntX) for j in elemLL])
        xLR = sum([abs(objectsCntX[j] - frameCntX) for j in elemLR])
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([xUL, xUR, xLL, xLR])
        if normValue:
            xUL = xUL/normValue
            xUR = xUR/normValue
            xLL = xLL/normValue
            xLR = xLR/normValue
        
        yUL = sum([abs(objectsCntY[j] - frameCntY) for j in elemUL])
        yUR = sum([abs(objectsCntY[j] - frameCntY) for j in elemUR])
        yLL = sum([abs(objectsCntY[j] - frameCntY) for j in elemLL])
        yLR = sum([abs(objectsCntY[j] - frameCntY) for j in elemLR])
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([yUL, yUR, yLL, yLR])
        if normValue:
            yUL = yUL/normValue
            yUR = yUR/normValue
            yLL = yLL/normValue
            yLR = yLR/normValue
        
        hUL = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"][j] for j in elemUL)
        hUR = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"][j] for j in elemUR)
        hLL = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"][j] for j in elemLL)
        hLR = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjHeight"][j] for j in elemLR)
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([hUL, hUR, hLL, hLR])
        if normValue:
            hUL = hUL/normValue
            hUR = hUR/normValue
            hLL = hLL/normValue
            hLR = hLR/normValue
        
        bUL = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"][j] for j in elemUL)
        bUR = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"][j] for j in elemUR)
        bLL = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"][j] for j in elemLL)
        bLR = sum(data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjWidth"][j] for j in elemLR)
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([bUL, bUR, bLL, bLR])
        if normValue:
            bUL = bUL/normValue
            bUR = bUR/normValue
            bLL = bLL/normValue
            bLR = bLR/normValue
        
        thUL = 0
        thUR = 0
        thLL = 0
        thLR = 0
        
        # If an element has the same x or y coordinate with the frame, do not include it in the calculations
        for j in elemUL:
            if not(objectsCntX[j]==frameCntX or objectsCntY[j]==frameCntY):
                thUL = thUL + abs((objectsCntY[j] - frameCntY)/(objectsCntX[j] - frameCntX))
        for j in elemUR:
            if not(objectsCntX[j]==frameCntX or objectsCntY[j]==frameCntY):
                thUR = thUR + abs((objectsCntY[j] - frameCntY)/(objectsCntX[j] - frameCntX))
        for j in elemLL:
            if not(objectsCntX[j]==frameCntX or objectsCntY[j]==frameCntY):
                thLL = thLL + abs((objectsCntY[j] - frameCntY)/(objectsCntX[j] - frameCntX))
        for j in elemLR:
            if not(objectsCntX[j]==frameCntX or objectsCntY[j]==frameCntY):
                thLR = thLR + abs((objectsCntY[j] - frameCntY)/(objectsCntX[j] - frameCntX))
        
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([thUL, thUR, thLL, thLR])
        if normValue:
            thUL = thUL/normValue
            thUR = thUR/normValue
            thLL = thLL/normValue
            thLR = thLR/normValue
        
        rUL = sum(sqrt(pow(objectsCntX[j] - frameCntX, 2) + pow(objectsCntY[j] - frameCntY, 2)) for j in elemUL) 
        rUR = sum(sqrt(pow(objectsCntX[j] - frameCntX, 2) + pow(objectsCntY[j] - frameCntY, 2)) for j in elemUR)
        rLL = sum(sqrt(pow(objectsCntX[j] - frameCntX, 2) + pow(objectsCntY[j] - frameCntY, 2)) for j in elemLL)
        rLR = sum(sqrt(pow(objectsCntX[j] - frameCntX, 2) + pow(objectsCntY[j] - frameCntY, 2)) for j in elemLR)
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([rUL, rUR, rLL, rLR])
        if normValue:
            rUL = rUL/normValue
            rUR = rUR/normValue
            rLL = rLL/normValue
            rLR = rLR/normValue
        
        
        SYMvert = (abs(xUL - xUR) + abs(xLL - xLR) + abs(yUL - yUR) + abs(yLL - yLR) +
					abs(hUL - hUR) + abs(hLL - hLR) + abs(bUL - bUR) + abs(bLL - bLR) +
					abs(thUL - thUR) + abs(thLL - thLR) + abs(rUL - rUR) + abs(rLL - rLR))/12   
                    
        SYMhoriz = (abs(xUL - xLL) + abs(xUR - xLR) + abs(yUL - yLL) + abs(yUR - yLR) +
					abs(hUL - hLL) + abs(hUR - hLR) + abs(bUL - bLL) + abs(bUR - bLR) +
					abs(thUL - thLL) + abs(thUR - thLR) + abs(rUL - rLL) + abs(rUR - rLR))/12 
                    
        SYMrad = (abs(xUL - xLR) + abs(xUR - xLL) + abs(yUL - yLR) + abs(yUR - yLL) +
				abs(hUL - hLR) + abs(hUR - hLL) + abs(bUL - bLR) + abs(bUR - bLL) +
				abs(thUL - thLR) + abs(thUR - thLL) + abs(rUL - rLR) + abs(rUR - rLL))/12 

        
        SYM.append(1 - (abs(SYMvert) + abs(SYMhoriz) + abs(SYMrad))/3)
            
        print(SYM[i])
        
    return SYM	


def rhythm(url, path):
    #Reading data 
    filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
    filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
    filename = path + filename + '_data.json'
    with open(filename, 'r') as f:
        data = json.load(f)  
        
    RHM = []
    print("\n")
    print("rhythm")
    frameCntX = data["scrapingResults"]["frameCenterX"]
    frameCntY = data["scrapingResults"]["frameCenterY"]
        
    for i in range(data["scrapingResults"]["numOfLayers"]):      
        
        objectCntX = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterX"]
        objectCntY = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjCenterY"]
        objectArea = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["ObjArea"]
        elemUL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UL"]["elemID"]
        elemUR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["UR"]["elemID"]
        elemLL = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LL"]["elemID"]
        elemLR = data["scrapingResults"]["elementAttributes"]["layer"+str(i)]["LR"]["elemID"]
        
        xUL = sum([abs(objectCntX[j] - frameCntX) for j in elemUL])
        xUR = sum([abs(objectCntX[j] - frameCntX) for j in elemUR])
        xLL = sum([abs(objectCntX[j] - frameCntX) for j in elemLL])
        xLR = sum([abs(objectCntX[j] - frameCntX) for j in elemLR])
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([xUL, xUR, xLL, xLR])
        if normValue:
            xUL = xUL/normValue
            xUR = xUR/normValue
            xLL = xLL/normValue
            xLR = xLR/normValue
        
        yUL = sum([abs(objectCntY[j] - frameCntY) for j in elemUL])
        yUR = sum([abs(objectCntY[j] - frameCntY) for j in elemUR])
        yLL = sum([abs(objectCntY[j] - frameCntY) for j in elemLL])
        yLR = sum([abs(objectCntY[j] - frameCntY) for j in elemLR])
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([yUL, yUR, yLL, yLR])
        if normValue:
            yUL = yUL/normValue
            yUR = yUR/normValue
            yLL = yLL/normValue
            yLR = yLR/normValue
          
        aUL = sum([objectArea[j] for j in elemUL])
        aUR = sum([objectArea[j] for j in elemUR])
        aLL = sum([objectArea[j] for j in elemLL])
        aLR = sum([objectArea[j] for j in elemLR])
        
        # Normalize values (if all of them are zero don't normalize)
        normValue = max([aUL, aUR, aLL, aLR])
        if normValue:
            aUL = aUL/normValue
            aUR = aUR/normValue
            aLL = aLL/normValue
            aLR = aLR/normValue
            
        RHMx = (abs(xUL - xUR) + abs(xUL - xLR) + abs(xUL - xLL) + abs(xUR - xLR) + abs(xUR - xLL) + abs(xLR - xLL))/6    
        RHMy = (abs(yUL - yUR) + abs(yUL - yLR) + abs(yUL - yLL) + abs(yUR - yLR) + abs(yUR - yLL) + abs(yLR - yLL))/6
        RHMarea = (abs(aUL - aUR) + abs(aUL - aLR) + abs(aUL - aLL) + abs(aUR - aLR) + abs(aUR - aLL) + abs(aLR - aLL))/6
        
        RHM.append(1 - (abs(RHMx) + abs(RHMy) + abs(RHMarea))/3)
            
        print(RHM[i])
        
    return RHM

