import re
import json
import numpy as np


# create data array for given urls of a specified layer
def create_data_array(urls, layer, path, target_names=None):
    data_array = []     #X
    category = []       #y (category index)
    
    #Reading data
    for idx,url in enumerate(urls):
        filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
        filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
        with open(path + filename+ '_results_processed.json', 'r') as f:
            data = json.load(f)
            
        data_array.append([])

        # metric values of web pages in given layer
        for metric in data["results"][url]["data"][layer]:
            data_array[idx].append(data["results"][url]["data"][layer][metric])

        if target_names is not None:
            # categories of web pages
            category.append(target_names.index(str(data["results"][url]["category"]))) 
        
    # Create np arrays of data
    X = np.array(data_array)        #features
    y = np.array(category)          #class
    
    return X, y






