import os
import numpy as np
import json

def collect_data(start_urls, categories, webpage_info, save_path):
    for url, category in zip(start_urls, categories):  
        
        #Collect data from the web pages and save in data folder   
        os.system('C:\\Users\\Sophie\\node_modules\\casperjs\\bin\\casperjs getAttributes.js %s %s'%(url, save_path))
        
        #Create json file with data needed in the next subsystems
        webpage_info["webpages"][url] = {}
        webpage_info["webpages"][url]["category"] = category
        
    return webpage_info

   
# MAIN DATA COLLECTOR CODE
if __name__ == '__main__':   
    news = ['http://www.nytimes.com/', 'http://www.foxnews.com/', 'http://www.wsj.com/europe', 'http://www.reuters.com/', 
            'http://www.latimes.com/', 'http://www.cnbc.com/world/?region=world', 'http://www.cbsnews.com/',
            'http://www.chron.com/','http://abcnews.go.com/', 'http://nypost.com/', 'http://www.sfgate.com/',
            'http://www.chinadaily.com.cn/', 'http://www.hollywoodreporter.com/', 'http://www.chicagotribune.com/', 
            'http://www.hindustantimes.com/', 'http://www.smh.com.au/', 'http://www.inquisitr.com/',
            'http://www.newsmax.com/', 'http://dailycaller.com/', 'http://www.theglobeandmail.com/',
            'http://www.cbc.ca/', 'http://www.indiatimes.com/', 'http://www.foxbusiness.com/','http://www.rappler.com/',
            'http://www.breakingnews.com/']
        
    shopping = ["https://www.amazon.com/", "http://www.ebay.com/",  "https://www.walmart.com/", "https://www.etsy.com/",
                "http://www.kmart.com/en_us/international-landing.html", "http://www.gap.com/", "http://www.kohls.com/", 
                "http://www.newegg.com/", "http://www.costco.com/", "https://www.lowes.com/", "http://www.jcpenney.com/",
                "https://www.bhphotovideo.com/", "http://www.toysrus.com/shop/index.jsp?categoryId=2255956",
                "http://www.gamestop.com/", "http://www.sears.com/en_intnl/dap/shopping-tourism.html",
                "http://www.ulta.com/", "http://www.trademe.co.nz/", "https://www.redbubble.com/", 
                "http://www.dickssportinggoods.com/home/index.jsp", "http://www.iherb.com/", 
                "http://www.samsclub.com/sams/homepage.jsp", "http://www.sephora.com/",
                "http://www.ulta.com/", "https://www.ifixit.com/", "http://www.rakuten.com/"]
        
    search_engines = ["http://www.google.com/", "https://search.yahoo.com/", "http://uk.ask.com/", "https://www.iconfinder.com/", 
                      "http://hp.myway.com/portal/ttab02/index.html", "https://duckduckgo.com/", "http://tineye.com/", 
                      "http://www.mysearch.com/", "http://www.webcrawler.com/", "http://www.wolframalpha.com/",
                      "http://info.com/", "http://www.dogpile.com/", "http://www.chacha.com/", "http://www.spokeo.com/",
                      "http://www.activesearchresults.com/", "http://www.scrubtheweb.com/", "http://lmgtfy.com/", 
                      "http://www.similarsites.com/", "http://www.similarsitesearch.com/", "http://search.creativecommons.org/",
                      "http://www.googleguide.com/", "http://compfight.com/", "http://www.refseek.com/", 
                      "http://www.sweetsearch.com/", "https://www.social-searcher.com/"]
    
    start_urls = []
    print len(news)
    print len(shopping)
    print len(search_engines)
    
    start_urls.extend(news)     
    start_urls.extend(shopping)
    start_urls.extend(search_engines)
        
    categories = ["news"]*len(news)
    categories.extend(["e-shopping"]*len(shopping))
    categories.extend(["search engine"]*len(search_engines))
    
    target_names = (np.unique(categories)).tolist()   #names of the categories used in the analysis  
        
        
    # COLLECT DATA
    
    # Create folder to store collected data if it does not already exist
    if not os.path.exists(os.getcwd()+"\\data"):
        os.makedirs(os.getcwd()+"\\data")   #current working directory + folder to create
    
    
    webpage_info  = {}
    webpage_info["target_names"] = target_names
    webpage_info["webpages"] = {}
        
    # Save results in 'data' file
    webpage_info = collect_data(start_urls, categories, webpage_info, 'data\\')
    
    # Create folder to store info data for the next subsystems
    if not os.path.exists(os.getcwd()+"\\system_data"):
        os.makedirs(os.getcwd()+"\\system_data")   #current working directory + folder to create
    
        
    json_results = json.dumps(webpage_info, indent = 4)        
    with open('system_data\\webpage_info.json', 'w') as f:
        f.write(json_results)
    
