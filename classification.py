import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree
from sklearn.model_selection import cross_val_predict
from sklearn.externals import joblib
from sklearn import metrics
import collections
import matplotlib.pyplot as plt
import json
import os
import random
   
    
def run_decisionTree(X, y, target_names, layer):    
    
    # Randomize data order
    random_data = list(zip(X, y))
    random.shuffle(random_data)
    X, y = zip(*random_data)
    X = np.array(X)
    y = np.array(y)
    
    # Classification process with decision tree algorithm  
    clf = tree.DecisionTreeClassifier(criterion='entropy', splitter='best', max_depth=None, 
                                      min_samples_split=2, min_samples_leaf=3, max_leaf_nodes=None)
    
    y_cross_val = cross_val_predict(clf, X, y, cv=10)
    clf = clf.fit(X,y)
    
    # print classification evaluation metrics    
    report = metrics.classification_report(y, y_cross_val, target_names=target_names)
    
    conf_mat = metrics.confusion_matrix(y, y_cross_val)
    accuracy = []
    for i, result in enumerate(conf_mat):
        accuracy.append(float(result[i])/sum(result))
    
    print (report)
    print ("--ACCURACY--")
    for i, target in enumerate(target_names):
        print (target + " : " + str(accuracy[i]))
    
    #plot_result_per_category(np.array(y).T, y_cross_val, target_names)
    
    # save model
    joblib.dump(clf, "models\\"+layer+'_DTclf.pkl')

    
    
def run_KNN(X, y, target_names, layer):        
    
    # Randomize data order
    random_data = list(zip(X, y))
    random.shuffle(random_data)
    X, y = zip(*random_data)
    X = np.array(X)
    y = np.array(y)
    
    # Classification process with KNN algorithm       
    
    for n_neighbors in [2,3,4,5]:
        clf = KNeighborsClassifier(n_neighbors=n_neighbors, metric='euclidean', weights='uniform', algorithm='brute')
        y_cross_val = cross_val_predict(clf, X, y, cv=10)
        
        clf = clf.fit(X,y)
        
        # print classification evaluation metrics    
        report = metrics.classification_report(y, y_cross_val, target_names=target_names)
        
        conf_mat = metrics.confusion_matrix(y, y_cross_val)
        accuracy = []
        for i, result in enumerate(conf_mat):
            accuracy.append(float(result[i])/sum(result))
        
        print (report)
        print ("--ACCURACY--")
        for i, target in enumerate(target_names):
            print (target + " : " + str(accuracy[i]))
            
        #plot_result_per_category(np.array(y).T, y_cross_val, target_names)
              
        # save model
        joblib.dump(clf, "models\\"+layer+'_KNNclf'+str(n_neighbors)+'.pkl')
        

    
    
def plot_result_per_category(y_test, y_pred, target_names):
    # Plot diagram to show the classification of the samples of each category
    for idx, true_category in enumerate(target_names):
        find_category_samples = [y_test == idx]  #find samples of the current category
        
        num_samples = np.sum(find_category_samples)
        
        res = y_pred[find_category_samples]
        res = collections.Counter(res)

        for i in res:
            res[i] = (float(res[i])/num_samples)*100    #percent of samples classified in each category
 
        width = 0.2       # the width of the bars
        ind = [0]
        for i in range(1,len(res)):
            ind.append(ind[i-1]+width+0.1)  # the x locations for the groups
            
        plt.figure()
        axes = plt.gca()
        
        colors = ['darkcyan', 'greenyellow', 'red', 'hotpink', 'gold', 'darkmagenta']
        
        axes.bar(ind, res.values(), width, align='center', color=colors[:len(res)])
        axes.set_ylabel('Percentage of samples classified in the category (%)')
        axes.set_title(true_category)
        axes.set_xticks(ind)
        labels = [target_names[i] for i in res.keys()]  #x axis labels are the categories into which the samples were classified
        axes.set_xticklabels(labels)
        axes.set_xlim([-0.6,ind[-1]+0.6])
        axes.set_ylim([0,110])

    plt.show()
        
   
    
# MAIN CLASSIFICATION CODE    
    
with open('system_data\\webpage_info.json', 'r') as f:
    webpage_info = json.load(f)
    
target_names = webpage_info["target_names"]           

with open('system_data\\dataset.json', 'r') as f:
    dataset = json.load(f)
    
i = 7   # run classification for layers: 0, 7, 9, 10, 12
layer = "layer_%02d" % i
X = dataset[layer]["X"]
y = dataset[layer]["y"]

# Create folder to store the classification results if it does not already exist
if not os.path.exists(os.getcwd()+"\\models"):
    os.makedirs(os.getcwd()+"\\models")   #current working directory + folder to create

#run_decisionTree(X, y, target_names, layer)
run_KNN(X, y, target_names, layer)

