var casper = require("casper").create();
casper.echo("hello casper");

function crawlTree(layerNum, keep, elem_tags, docWidth, docHeight) {
	
	var array = casper.evaluate(function(layerNum, keep, elem_tags, docWidth, docHeight){
		/* iterative process to create layers */
		var x = document.body.children;		//select body's direct children
		var y = [];							//array to save children in the following layers
		var numChildren = x.length;			//number of children of body
		
		for(var i=0;i<layerNum;i++){		//layerNum=0 the first time, so the loop isn't executed
			numChildren = [];				//if layerNum>0 find children to create the next layer
			
			for (var j=0;j<keep["layer"+i].length;j++){	//find children only of the elements we kept in the previous layer	
				if (elem_tags["layer"+i][j]=='IFRAME' || elem_tags["layer"+i][j]=='svg'){
					// if the element's tag is "iframe" or "svg" don't find its children
					numChildren[j] = 0;
					continue;
				}else{
					// save the number of children each element has in the next layer (helps to find them later)
					numChildren[j] = x[parseInt(keep["layer"+i][j])].children.length;
				}
								
				Array.prototype.forEach.call(x[keep["layer"+i][j]].children, function(e) {
					y.push(e);		//save children of each element as a DOM element object in y array
				});	
			}	
			x = y;			//x holds the elements of the layer we just created and will be used to create the next one
			y = [];			//y becomes empty for the next run  
			//if i<current layerNum, run the loop again to find children one layer deeper until we reach the desired layer
		}
		// create array with the indexes of the children of each element
		var ElemChildren = [];
		var sum = 0;
		for (i=0; i<numChildren.length; i++){
			ElemChildren[i] = [];
			for (j=sum; j<sum+numChildren[i]; j++){
				ElemChildren[i].push(j);
			}
			sum += numChildren[i];
		}
		
		// arrays to save the width, height and the exact coordinates of the elements in this layer
		var widthArray = [];
		var heightArray = [];
		var coordinates = [];
		keep["layer"+layerNum] = [];		// save indexes of the elements we keep based on conditions
		elem_tags["layer"+layerNum] = [];	// save tags of the elements we keep
		
		/* For each child in this layer, find property values */		
		Array.prototype.forEach.call(x, function(e,index) {
			
			var dims = e.getBoundingClientRect();
			
			var elemCoords = {top: dims.top + window.scrollY,
							bottom: dims.bottom + window.scrollY,
							left: dims.left + window.scrollX,
							right: dims.right + window.scrollX};
			
			//Remove padding from element coordinates to get the exact size of the element
			
			if (dims.width!=0){
				var style = window.getComputedStyle(e, null);
				var padding = {
					left: parseInt(style.getPropertyValue("padding-left")),
					right: parseInt(style.getPropertyValue("padding-right")),
				};
				
				elemCoords.left = elemCoords.left + padding.left;
				elemCoords.right = elemCoords.right - padding.right;
			}
			if (dims.height!=0){
				
				var style = window.getComputedStyle(e, null);
				var padding = {
					top: parseInt(style.getPropertyValue("padding-top")),
					bottom: parseInt(style.getPropertyValue("padding-bottom")),
				};

				elemCoords.top = elemCoords.top + padding.top;
				elemCoords.bottom = elemCoords.bottom - padding.bottom;				
			}
			
			//if the element is not displayed, has irrelevant tag, or is out of the bounds of the document, reject it
			if ((e.tagName=='META') || (e.tagName=='BR') || (e.tagName=='SCRIPT') || (e.tagName=='NOSCRIPT') || (e.style.visibility=='hidden') || (e.style.display=='none') || (elemCoords.bottom<0) || (elemCoords.right<0) || (elemCoords.left>docWidth) || (elemCoords.top>docHeight)){
				for(var z=0; z < ElemChildren.length; z++){
					var indexOfChild = ElemChildren[z].indexOf(index);	//find the index of the z child in the keep array
					if(indexOfChild!==-1){
						ElemChildren[z].splice(indexOfChild, 1);
					}
				}
			}
			else{
				
				if(elemCoords.top < 0){
					elemCoords.top = 0;
				}
				if(elemCoords.left < 0){
					elemCoords.left = 0;
				}
				if(elemCoords.right > docWidth){
					elemCoords.right = docWidth;
				}
				if(elemCoords.bottom > docHeight){
					elemCoords.bottom = docHeight;
				}
				
				var width = elemCoords.right - elemCoords.left;
				var height = elemCoords.bottom - elemCoords.top;
				
				widthArray.push(width);
				heightArray.push(height);
				coordinates.push(elemCoords);
				keep["layer"+layerNum].push(index);	
				elem_tags["layer"+layerNum].push(e.tagName);
			}			
		});

		var data;
		
		/* if no element was kept in this layer return null data */
		if (keep["layer"+layerNum].length==0){
			data = null;
		}else{
			data = {ElemChildren:ElemChildren, NumOfElements:keep["layer"+layerNum].length, ObjWidth:widthArray, ObjHeight:heightArray, ObjCoords:coordinates};
		}	
		
		/* return array of results to getDim function*/  
		return Array.prototype.map.call([data, keep, elem_tags], function(e) {
			return e;
		});
					
	},layerNum, keep, elem_tags, docWidth, docHeight);
		
	return array;
}	


/* find frame dimensions */
function getDocumentDim() {

	return [__utils__.getDocumentWidth(), __utils__.getDocumentHeight()];
}

/* Calculate dimensions for the elements with height or width = 0 */
function calcDim(dataArray, numOfLayers) {
	
	var topPtn, bottomPtn, leftPtn, rightPtn;
											//the layer with index=numOfLayers is empty
	for(var i=numOfLayers-2;i>-1;i--){		//numOfLayers-1 is the last layer, so move to the one before the last 
		var child_index = 0;	//used to point to the correct index of the child we want to check
		
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){  	//find children in the next layer for each element we kept in this layer
			
			// if we reach the last child element, we don't need to continue examiniming the rest of the elements in the layer
			if(child_index >= dataArray[i+1]["NumOfElements"]){
				break;
			}
			/* if width,height!=0 we don't need to calculate them based on their children */
			if(dataArray[i]["ObjWidth"][j]!==0 && dataArray[i]["ObjHeight"][j]!==0){
				child_index = child_index + dataArray[i+1]["ElemChildren"][j].length;		//add numOfChildren to sum to find the index of the first child of the next element
				continue;
			}
			
			var firstTime = true;
			
			for(var z = child_index; z < child_index + dataArray[i+1]["ElemChildren"][j].length ;z++){
				if(firstTime){	// in the first run the variables equal the values of the first element we kept
					firstTime = false;
					topPtn = dataArray[i+1]["ObjCoords"][z]["top"];
					bottomPtn = dataArray[i+1]["ObjCoords"][z]["bottom"];
					leftPtn = dataArray[i+1]["ObjCoords"][z]["left"];
					rightPtn = dataArray[i+1]["ObjCoords"][z]["right"];
				}
				if(dataArray[i+1]["ObjCoords"][z]["top"]<topPtn){	//find the top-most point of the children
					topPtn = dataArray[i+1]["ObjCoords"][z]["top"];
				}
				if(dataArray[i+1]["ObjCoords"][z]["bottom"]>bottomPtn){	//find the bottom-most point of the children
					bottomPtn = dataArray[i+1]["ObjCoords"][z]["bottom"];
				}
				if(dataArray[i+1]["ObjCoords"][z]["left"]<leftPtn){	//find the left-most point of the children
					leftPtn = dataArray[i+1]["ObjCoords"][z]["left"];
				}
				if(dataArray[i+1]["ObjCoords"][z]["right"]>rightPtn){	//find the right-most point of the children
					rightPtn = dataArray[i+1]["ObjCoords"][z]["right"];
				}
				
			}

			// calculate width and height based on the element's coordinates calculated by his children
			if(dataArray[i]["ObjWidth"][j]===0 && dataArray[i+1]["ElemChildren"][j].length!=0){
				dataArray[i]["ObjWidth"][j] = rightPtn - leftPtn;
				dataArray[i]["ObjCoords"][j]["left"] = leftPtn;
				dataArray[i]["ObjCoords"][j]["right"] = rightPtn;
			}
			if(dataArray[i]["ObjHeight"][j]===0 && dataArray[i+1]["ElemChildren"][j].length!=0){
				dataArray[i]["ObjHeight"][j] = bottomPtn - topPtn;
				dataArray[i]["ObjCoords"][j]["top"] = topPtn;
				dataArray[i]["ObjCoords"][j]["bottom"] = bottomPtn;
			}
			
			child_index += dataArray[i+1]["ElemChildren"][j].length; 	//update sum to find the index of the first child of the next element
		}
		
	}
	
	return dataArray;

}



/* Calculate area for all elements */
function calcArea( dataArray, numOfLayers ){
	
	for (var i=0;i<numOfLayers;i++){	//for each layer 
		areaArray = [];
		for (var j=0;j<dataArray[i]["NumOfElements"];j++){		//for each element in the layer
			areaArray.push(dataArray[i]["ObjWidth"][j]*dataArray[i]["ObjHeight"][j]);	//compute area
		}
		dataArray[i]["ObjArea"] = areaArray;
	}
	
	return dataArray;
}



/* Repeat elements with non-zero area and no children in the next layers */
function repeatElem(dataArray, numOfLayers){
	
	var count = 0;	//count holds the number of elements that are repeated in the next layers

	for(var i=0;i<numOfLayers-1;i++){	
		
		// repeat already eligible elements in the next layer (the repeated elements are in the last positions of each layer in the dataArray)
		// for layer0 there are no elements from previous layers to repeat, so the next loop is not executed
		for(var j=dataArray[i]["NumOfElements"]-count;j<dataArray[i]["NumOfElements"];j++){
			for(var z in dataArray[i]){	//copy all the useful properties in the dataArray for the elements that are already repeated
				if(z!=='NumOfElements' && z!=='ElemChildren'){
					dataArray[i+1][z].push(dataArray[i][z][j]);
				}
			}	
		}
		
		
		
		// check the elements which were kept in this layer (not the ones that were repeated from previous layers)
		for(j=0; j < dataArray[i]["NumOfElements"]-count ;j++){	
					
			//if the element has no children in the next layer and area!=0, include it in the next layer
			if((dataArray[i+1]["ElemChildren"][j].length==0) && (dataArray[i]["ObjArea"][j]!==0)){
				for(z in dataArray[i]){	//copy all the useful properties
					if(z!=='NumOfElements' && z!=='ElemChildren'){
						dataArray[i+1][z].push(dataArray[i][z][j]);
					}
				}
				count++;
			}	
		}	
		// update the number of elements in the layer 
		dataArray[i+1]["NumOfElements"] = dataArray[i+1]["NumOfElements"]+count;
		
		/*if (dataArray[i+1]["NumOfElements"]!==dataArray[i+1]["ObjArea"].length){
			casper.echo("VERIFICATION PROBLEM!!!");		//verify the number of elements in each layer
		}*/
		
	}
	
	return dataArray;
	
}

 

/* Eliminate elements with area=0 */ 
function findUsefulElem(dataArray, numOfLayers){
	
	for(i=0;i<numOfLayers;i++){	// for each layer
		var index = [];
		var count = 0;
		if(dataArray[i]["ObjArea"].length!==dataArray[i]["NumOfElements"]){
			casper.echo("mistake!");
		}
		
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){
			if(dataArray[i]["ObjArea"][j]==0){		// check if there are elements with area=0
					index.push(j);		//add their index to the index array
					
			}
		}	
		for(var z=0;z<index.length;z++){	//for each element in the index array
			for(var y in dataArray[i]){
				if(y!=='NumOfElements' && y!=='ElemChildren'){
					dataArray[i][y].splice(index[z]-z, 1);	//remove it from the data
				}
			}
			
		}
		//NumOfElements is its previous value minus number of elements which were deleted
		dataArray[i]["NumOfElements"] = dataArray[i]["NumOfElements"] - index.length;

	}
	
	
	return dataArray;
}




/* Find layout dimensions (smallest box that fits all elements) */ 
function layoutDims(dataArray, numOfLayers){
	for(var i=0;i<numOfLayers;i++){
		
		var leftPtn, rightPtn, topPtn, bottomPtn;
		
		// find left-most, right-most, top-most and bottom-most points of the elements
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){	
			if (j==0){
				leftPtn = dataArray[i]["ObjCoords"][j]["left"];
				rightPtn = dataArray[i]["ObjCoords"][j]["right"];
				topPtn = dataArray[i]["ObjCoords"][j]["top"];
				bottomPtn = dataArray[i]["ObjCoords"][j]["bottom"];
			}
			else{
				if (dataArray[i]["ObjCoords"][j]["left"] < leftPtn){
					leftPtn = dataArray[i]["ObjCoords"][j]["left"];
				}
				if (dataArray[i]["ObjCoords"][j]["right"] > rightPtn){
					rightPtn = dataArray[i]["ObjCoords"][j]["right"];
				}
				if (dataArray[i]["ObjCoords"][j]["top"] < topPtn){
					topPtn = dataArray[i]["ObjCoords"][j]["top"];
				}
				if (dataArray[i]["ObjCoords"][j]["bottom"] > bottomPtn){
					bottomPtn = dataArray[i]["ObjCoords"][j]["bottom"];
				}
			}			
		}
		dataArray[i]["layoutWidth"] = rightPtn - leftPtn;
		dataArray[i]["layoutHeight"] = bottomPtn - topPtn;
		dataArray[i]["layoutArea"] = dataArray[i]["layoutWidth"]*dataArray[i]["layoutHeight"];
	}
	return dataArray;
}



/* Calculate center of the elements */
function elemCenter( dataArray, numOfLayers ){
	
	for(var i=0;i<numOfLayers;i++){
		dataArray[i]["ObjCenterX"] = [];
		dataArray[i]["ObjCenterY"] = [];
		// calculate the center points of all elements
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){	
			// the centers are the half of each dimension added to the starting points of the element 
			dataArray[i]["ObjCenterX"].push(dataArray[i]["ObjCoords"][j]["left"] + (dataArray[i]["ObjWidth"][j]/2));
			dataArray[i]["ObjCenterY"].push(dataArray[i]["ObjCoords"][j]["top"] + (dataArray[i]["ObjHeight"][j]/2));
		}
	}	
		
	return dataArray;
}

/* Find info for the four parts of the screen(top, bottom, left, right) seperately */
function partsOfScreen( dataArray, numOfLayers ){
	var frameDims = casper.evaluate(getDocumentDim);
	var vertMiddle = frameDims[0]/2;	// middle of the screen vertically = width/2
	var horizMiddle = frameDims[1]/2;	// middle of the screen horizontally = height/2
	
	for(var i=0;i<numOfLayers;i++){
		dataArray[i]["ElemTopArea"] = []
		dataArray[i]["ElemBottomArea"] = []
		dataArray[i]["ElemLeftArea"] = []
		dataArray[i]["ElemRightArea"] = []
		
		dataArray[i]["ElemDistTop"] = []
		dataArray[i]["ElemDistBottom"] = []
		dataArray[i]["ElemDistLeft"] = []
		dataArray[i]["ElemDistRight"] = []
		
		dataArray[i]["TopNumElements"] = 0;
		dataArray[i]["BottomNumElements"] = 0;
		dataArray[i]["LeftNumElements"] = 0;
		dataArray[i]["RightNumElements"] = 0;
		
		// calculate the area of every element that belongs to each part of the screen
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){	
		
			if(dataArray[i]["ObjCenterY"][j]<horizMiddle){	
				dataArray[i]["ElemTopArea"].push(dataArray[i]["ObjArea"][j]);
				dataArray[i]["ElemDistTop"].push(horizMiddle - dataArray[i]["ObjCenterY"][j]);
				dataArray[i]["TopNumElements"]++;			
			}
			else if(dataArray[i]["ObjCenterY"][j]>horizMiddle){
				dataArray[i]["ElemBottomArea"].push(dataArray[i]["ObjArea"][j]);
				dataArray[i]["ElemDistBottom"].push(dataArray[i]["ObjCenterY"][j] - horizMiddle);				
				dataArray[i]["BottomNumElements"]++;
			}
			else{
				// if the element is on the boundary, include it in the top part
				dataArray[i]["ElemTopArea"].push(dataArray[i]["ObjArea"][j]);
				dataArray[i]["ElemDistTop"].push(0);
				dataArray[i]["TopNumElements"]++;
			}
			
			if(dataArray[i]["ObjCenterX"][j]<vertMiddle){
				dataArray[i]["ElemLeftArea"].push(dataArray[i]["ObjArea"][j]);
				dataArray[i]["ElemDistLeft"].push(vertMiddle - dataArray[i]["ObjCenterX"][j]);
				dataArray[i]["LeftNumElements"]++;				
			}
			else if(dataArray[i]["ObjCenterX"][j]>vertMiddle){
				dataArray[i]["ElemRightArea"].push(dataArray[i]["ObjArea"][j]);				
				dataArray[i]["ElemDistRight"].push(dataArray[i]["ObjCenterX"][j] - vertMiddle);
				dataArray[i]["RightNumElements"]++;

			}
			else{
				// if the element is on the boundary, include it in the left part
				dataArray[i]["ElemLeftArea"].push(dataArray[i]["ObjArea"][j]);
				dataArray[i]["ElemDistLeft"].push(0);
				dataArray[i]["LeftNumElements"]++;	
			}
			
		}
	}	
	return dataArray;
	
}




/* Find alignment points of elements and distances between them */
function alignmentPoints( dataArray, numOfLayers ){	
	for(var i=0;i<numOfLayers;i++){
		// alignment points referring to all sides of the elements
		var allVertPoints = [];
		var allHorizPoints = [];
		// alignment points referring to the top and the left side of the elements (starting points)
		var startingVertPoints = [];
		var startingHorizPoints = [];
		
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){	
			if(allVertPoints.indexOf(dataArray[i]["ObjCoords"][j]["left"]) === -1){
				allVertPoints.push(dataArray[i]["ObjCoords"][j]["left"]);
				startingVertPoints.push(dataArray[i]["ObjCoords"][j]["left"]);
			}
			if(allVertPoints.indexOf(dataArray[i]["ObjCoords"][j]["right"]) === -1){
				allVertPoints.push(dataArray[i]["ObjCoords"][j]["right"]);
			}
			if(allHorizPoints.indexOf(dataArray[i]["ObjCoords"][j]["top"]) === -1){
				allHorizPoints.push(dataArray[i]["ObjCoords"][j]["top"]);
				startingHorizPoints.push(dataArray[i]["ObjCoords"][j]["top"]);
			}
			if(allHorizPoints.indexOf(dataArray[i]["ObjCoords"][j]["bottom"]) === -1){
				allHorizPoints.push(dataArray[i]["ObjCoords"][j]["bottom"]);
			}
	
		}
		
		var colDistances = [];
		var rowDistances = [];
		
		// Sort points before calculating discrete distances between them
		startingVertPoints.sort();
		startingHorizPoints.sort();
		
		for(j=0;j<startingVertPoints.length-1;j++){
			if(colDistances.indexOf(Math.abs(startingVertPoints[j+1]-startingVertPoints[j])) === -1){
				colDistances.push(Math.abs(startingVertPoints[j+1]-startingVertPoints[j]));
			}
		}
		for(j=0;j<startingHorizPoints.length-1;j++){
			if(rowDistances.indexOf(Math.abs(startingHorizPoints[j+1]-startingHorizPoints[j])) === -1){
				rowDistances.push(Math.abs(startingHorizPoints[j+1]-startingHorizPoints[j]));
			}
		}
		
		dataArray[i]["allVap"] = allVertPoints.length;
		dataArray[i]["allHap"] = allHorizPoints.length;
		
		dataArray[i]["StartingVap"] = startingVertPoints.length;
		dataArray[i]["StartingHap"] = startingHorizPoints.length;
		
		dataArray[i]["RowColSpacing"] = colDistances.length + rowDistances.length;

	}	
		
	return dataArray;
	
}


function quadrants( dataArray, numOfLayers ){
	var frameDims = casper.evaluate(getDocumentDim);
	var frameWidth = frameDims[0];
	var frameHeight = frameDims[1];
	var vertMiddle = frameWidth/2;	// middle of the screen vertically = width/2
	var horizMiddle = frameHeight/2;	// middle of the screen horizontally = height/2
	
	
	for(var i=0;i<numOfLayers;i++){
		
		dataArray[i]["UL"] = {}
		dataArray[i]["UR"] = {}
		dataArray[i]["LL"] = {}
		dataArray[i]["LR"] = {}
		// save number of elements in each quadrant
		dataArray[i]["UL"]["numOfElems"] = 0
		dataArray[i]["UR"]["numOfElems"] = 0
		dataArray[i]["LL"]["numOfElems"] = 0
		dataArray[i]["LR"]["numOfElems"] = 0
		// save indexes of the elements in each quadrant to get their attributes later
		dataArray[i]["UL"]["elemID"] = []
		dataArray[i]["UR"]["elemID"] = []
		dataArray[i]["LL"]["elemID"] = []
		dataArray[i]["LR"]["elemID"] = []
		// save area of the elements in each quadrant
		dataArray[i]["UL"]["objArea"] = []
		dataArray[i]["UR"]["objArea"] = []
		dataArray[i]["LL"]["objArea"] = []
		dataArray[i]["LR"]["objArea"] = []

		var elemCoords, elemArea;
		
		for(var j=0;j<dataArray[i]["NumOfElements"];j++){	
		
			elemCoords = dataArray[i]["ObjCoords"][j];
			// an element belongs to a quadrant if its center is in the quadrant
			if(dataArray[i]["ObjCenterX"][j]<vertMiddle && dataArray[i]["ObjCenterY"][j]<horizMiddle){	
				dataArray[i]["UL"]["numOfElems"]++;
				dataArray[i]["UL"]["elemID"].push(j);
				
				elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
				dataArray[i]["UL"]["objArea"].push(elemArea);
			}
			else if (dataArray[i]["ObjCenterX"][j]>vertMiddle && dataArray[i]["ObjCenterY"][j]<horizMiddle){
				dataArray[i]["UR"]["numOfElems"]++;
				dataArray[i]["UR"]["elemID"].push(j);
				
				elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(elemCoords.right - Math.max(elemCoords.left, vertMiddle));
				dataArray[i]["UR"]["objArea"].push(elemArea);
			}
			else if (dataArray[i]["ObjCenterX"][j]<vertMiddle && dataArray[i]["ObjCenterY"][j]>horizMiddle){
				dataArray[i]["LL"]["numOfElems"]++;
				dataArray[i]["LL"]["elemID"].push(j);
				
				elemArea = (elemCoords.bottom - Math.max(horizMiddle, elemCoords.top))*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
				dataArray[i]["LL"]["objArea"].push(elemArea);
			}
			else if (dataArray[i]["ObjCenterX"][j]>vertMiddle && dataArray[i]["ObjCenterY"][j]>horizMiddle){
				dataArray[i]["LR"]["numOfElems"]++;
				dataArray[i]["LR"]["elemID"].push(j);
				
				elemArea = (elemCoords.bottom - Math.max(horizMiddle, elemCoords.top))*(elemCoords.right - Math.max(elemCoords.left, vertMiddle));
				dataArray[i]["LR"]["objArea"].push(elemArea);
				
			}	// if the element is on the boundary, include it a the predefined quadrant
			else if ( dataArray[i]["ObjCenterX"][j]<vertMiddle && dataArray[i]["ObjCenterY"][j]==horizMiddle ){	
				dataArray[i]["UL"]["numOfElems"]++;
				dataArray[i]["UL"]["elemID"].push(j);
					
				elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
				dataArray[i]["UL"]["objArea"].push(elemArea);
			}
			else if ( dataArray[i]["ObjCenterX"][j]>vertMiddle && dataArray[i]["ObjCenterY"][j]==horizMiddle ){	
				dataArray[i]["UR"]["numOfElems"]++;
				dataArray[i]["UR"]["elemID"].push(j);
					
				elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(elemCoords.right - Math.max(elemCoords.left, vertMiddle));
				dataArray[i]["UR"]["objArea"].push(elemArea);
			}
			else if ( dataArray[i]["ObjCenterX"][j]==vertMiddle && dataArray[i]["ObjCenterY"][j]>horizMiddle ){	
				dataArray[i]["LL"]["numOfElems"]++;
				dataArray[i]["LL"]["elemID"].push(j);
					
				elemArea = (elemCoords.bottom - Math.max(horizMiddle, elemCoords.top))*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
				dataArray[i]["LL"]["objArea"].push(elemArea);
				
			}
			else if ( dataArray[i]["ObjCenterX"][j]==vertMiddle && dataArray[i]["ObjCenterY"][j]<horizMiddle ){	
				dataArray[i]["UL"]["numOfElems"]++;
				dataArray[i]["UL"]["elemID"].push(j);
				
				elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
				dataArray[i]["UL"]["objArea"].push(elemArea);
			}
			else{ //if it is exactly on the center of the frame, include it in the upper-left quadrant
					dataArray[i]["UL"]["numOfElems"]++;
					dataArray[i]["UL"]["elemID"].push(j);
					
					elemArea = (Math.min(elemCoords.bottom, horizMiddle) - elemCoords.top)*(Math.min(elemCoords.right, vertMiddle) - elemCoords.left);
					dataArray[i]["UL"]["objArea"].push(elemArea);
			}
			
		}
				
	}	
	return dataArray;
	
}



function createJson( dataArray, numOfLayers ){
	
	var data = {};
	data["scrapingResults"] = {};
	data["scrapingResults"]["name"] = casper.cli.args[0];
	data["scrapingResults"]["numOfLayers"] = numOfLayers;
	
	var frameDims = casper.evaluate(getDocumentDim);	//get dimensions of layout
	data["scrapingResults"]["frameWidth"] = frameDims[0];
	data["scrapingResults"]["frameHeight"] = frameDims[1];
	data["scrapingResults"]["frameArea"] = frameDims[0]*frameDims[1];
	data["scrapingResults"]["frameCenterX"] = frameDims[0]/2; // centerX = frameWidth/2
	data["scrapingResults"]["frameCenterY"] = frameDims[1]/2; // centerY = frameHeight/2
	
	data["scrapingResults"]["elementAttributes"] = {};
	
	// copy the data object that holds information for each layer as it was created earlier
	for(var i=0;i<numOfLayers;i++){
		data["scrapingResults"]["elementAttributes"]["layer"+i] = dataArray[i];
	}

	return data;
}


function overlappingElems(dataArray, numOfLayers){
		
	// delete elements that overlap entirely with another element
	for(var i=0;i<numOfLayers;i++){
		var flag = false;
		var j = 0;
		do{
			for(var k=0; k<dataArray[i]["NumOfElements"];k++){
				if(j==k){
					continue;
				}
				if(dataArray[i]["ObjCoords"][j].right <= dataArray[i]["ObjCoords"][k].right && 
					dataArray[i]["ObjCoords"][j].left >= dataArray[i]["ObjCoords"][k].left && 
					dataArray[i]["ObjCoords"][j].bottom <= dataArray[i]["ObjCoords"][k].bottom && 
					dataArray[i]["ObjCoords"][j].top >= dataArray[i]["ObjCoords"][k].top){
					casper.echo("delete");
					for(var y in dataArray[i]){
						if(y!=='NumOfElements' && y!=='ElemChildren'){
							dataArray[i][y].splice(j, 1);	//remove it from the data
						}
					}
					dataArray[i]["NumOfElements"] = dataArray[i]["NumOfElements"] - 1;
					flag = true;
					break;
					
				}
			}
			if(flag){
				// i is the same because when the element was deleted, the rest of them moved one position to the left
				flag = false;
			}
			else{
				j++;
			}
			
		}while (j < dataArray[i]["NumOfElements"]);
	}	
	return dataArray;
}

// start loading web page with casper
casper.start(casper.cli.args[0]).viewport(1366,768); //most used screen resolution in 2016


casper.then(function() {
	
	//wait for 5 seconds to load page before scraping it    
	this.wait(5000, function() { 
		
		// Get document size
		docDims = this.evaluate(getDocumentDim);
		//this.viewport.width = docDims[0];
		//this.viewport(docDims[0], docDims[1]);
		
		var results;
		var dataArray = [];
		var keep = {};	
		var elem_tags = {};
		var currentLayer = 0;

		// Call crawlTree function to get information for each layer
		do {
			results = crawlTree(currentLayer, keep, elem_tags, docDims[0], docDims[1]);
			dataArray[currentLayer] = results[0];	//array with info objects
			keep = results[1];			//object with the elements we kept in the each layer
			elem_tags = results[2];
			currentLayer += 1;
			//stop if no element was kept in the previous layer	
		} while (keep["layer"+(currentLayer-1)].length!==0);	

		var numOfLayers = currentLayer-1;
		
		this.echo("\nNumber of layers = "+numOfLayers+"\n");
		
		/* Calculate dimensions of elements with area=0 based on their children */
		dataArray = calcDim(dataArray, numOfLayers);
		this.echo("Dimensions calculated!");
		
		/* Calculate area of elements */
		dataArray = calcArea(dataArray, numOfLayers);
		this.echo("Area calculated!");
		
		/* Repeat elements as they are in the next layer if they don't have any children */
		dataArray = repeatElem(dataArray, numOfLayers );
		this.echo("Element properties appended");
		
		/* Reject elements with area=0 */
		dataArray = findUsefulElem(dataArray, numOfLayers);
		this.echo("Useful elements found");
		
		/* Delete overlapping elements */
		//dataArray = overlappingElems(dataArray, numOfLayers);
		//this.echo("overlapping elements deleted!");
		
		/* Find layout dimensions (smallest box that fits all elements) */
		dataArray = layoutDims(dataArray, numOfLayers);
		this.echo("Layout dimensions found");
		
		/* Find center of elements */
		dataArray = elemCenter( dataArray, numOfLayers );
		this.echo("Centers found");
		
		/* Find info for the four parts of the screen seperately */
		dataArray = partsOfScreen( dataArray, numOfLayers );
		this.echo("Screen divided");
		
		/* Find alignment points */
		dataArray = alignmentPoints( dataArray, numOfLayers );
		this.echo("Alignment points found");

		/* Find number of elements in each quadrant */
		dataArray = quadrants( dataArray, numOfLayers )
		this.echo("Quadrants done!");
			
		/* Store data to json file */
		var data = createJson(dataArray, numOfLayers);
		this.echo("json created!");
		var str = JSON.stringify(data, null, 4);  //convert object to json format
		var fs = require('fs');
		
		// Create filename based on URL's name
		regex = /.*?\/\/(www)?(.*)\./;
		regex.compile(regex);
		var filename = (this.cli.args[0]).split(regex)[2];
		regex = /^\./
		regex.compile(regex);
		filename = filename.replace(regex,"")
		filename = this.cli.args[1]+filename + '_data.json'
		this.echo(filename);
		// Write to file
		fs.write(filename, str);
	});
});



casper.run();