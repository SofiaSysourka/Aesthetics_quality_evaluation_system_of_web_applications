# Introduction

With the web applications constantly growing in number, the competition between them increases significantly. As a result, web application providers strive to build user interfaces that offer attractive aesthetical design and ease of navigation.

This project aims to answer to the question:
> How can the aesthetic design of a webpage GUI be evaluated?

We contribute to the aforementioned question by identifying design patterns related to the aesthetic characteristics of the GUIs, as well as specialized patterns which are implemented on webpages of specified content. The process of finding the aforementioned design patterns is based on the way end-users perceive aesthetics (user-perceived aesthetics), indirectly reflected on web application popularity.

Towards this end, a data collection and processing system is developed, that leeds to the development of an evaluation model of the aesthetic design quality of the webpages.Static analysis is performed on the chosen webpages in order to collect useful information regarding the GUI components (i.e. the number of elements in each webpage and the way they are distributed among the layers of view), as well as the calculation of a series of metrics used widely in bibliography.

Classification and clustering techniques are applied on the collected data which result in the development of a combined aesthetics evaluation model. 

# How to use
The system consists of the following subsystems:
- Data Collector
- Metric Calculator
- Data Analyzer
- Webpage UI Quality Estimator

Each subsystem is described in detail in the next section.

## Data Collector
The _Data Collector (data\_collector.py, getAttributes.js)_ is used to collect the necessary data from the chosen webpages in order to calculate the aesthetics metrics later. As it was mentioned earlier, the webpages we choose for the analysis must be popular ones. The _Data Collector_ already includes 75 popular webpages of 3 different domains (news, e-shopping, search engines), but more can be added. Data are collected for each layer of the webpage HTML tree separately.

The scraped data as well as some useful information to use in the next subsystems are saved in the folder _"system data"_ of the current working directory as json files.

```python
# Create folder to store info data for the next subsystems
    if not os.path.exists(os.getcwd()+"\\system_data"):
        os.makedirs(os.getcwd()+"\\system_data")   #current working directory + folder to create
```
## Metric Calculator
The _Metric Calculator (metric\_calculator.py, Metrics.py)_ uses the scraped data to calculate a series of aesthetics metrics. More metrics can be added in the Metrics.py. The results are saved in the folder _"data"_ of the current working directory as json files.

## Data Analyzer
The _Data Analyzer (data\_analysis.py)_ processes the metric values calculated in the previous step to prepare them for further analysis. This analysis includes:
- Correlation between the domain categories
- Correlation between the metrics for the full dataset
- Correlation between layers
- Metric value range

## Webpage UI Quality Estimator
The _Webpage UI Quality Estimator (classification.py , clustering.py , evaluation.py)_ is responsible for the webpage evaluation. The procedure is as follows:
1. Clustering techniques are used to determine the layers where the different domain categories are best separated.
2. A classification model is built for each of the best layers.
3. New webpages are chosen for evaluation. They go through the whole process of collecting data, calculating metrics and proccesing.
4. The classification models are used on each webpage for each best layer and the results are combined using probabilities into a final result.

Before initiating the procedure, the data are preprocessed in the folowing way:
1. Fill missing values.
2. Reject outliers.
3. Scale and normalize the features.
4. Randomize data order.
5. Estimate optimal number of features.

## Result
If the domain category of a webpage is predicted correctly, we assume that its aesthetic design is according to the high quality standards of popular webpages.
If the prediction is incorrect, improvements must be made on the aesthetic design.

