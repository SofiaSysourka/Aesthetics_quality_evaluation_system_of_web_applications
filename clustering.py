from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import collections
import random
import json
import os

from sklearn import preprocessing
from sklearn.svm import OneClassSVM

from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN
from sklearn import metrics

from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import RFECV

import auxiliary_functions

 


def run_kmeans(X, y, target_names):   
        
    # Clustering Metrics
    adjusted_rand = []
    fowlkes_mallows = []
    calinski_harabaz = []
    homogeneity = []
    completeness = []
    v_measure = []
    
    n_clusters = 3
    
    max_iter = 300
    n_init = 10
    
    # Initialize the clusterer to implement Kmeans algorithm
    clusterer = KMeans(n_clusters=n_clusters, init='k-means++', max_iter=max_iter, n_init=n_init)
    cluster_labels = clusterer.fit_predict(X)   
                               
    # plot silhouette metric values
    plot_silhouette(X, n_clusters, cluster_labels, 'K-means')
        
    # plot clustering result
    for cluster_id in range(0,n_clusters):
        y = np.array(y)
        find_samples = y[cluster_labels == cluster_id]
        result = collections.Counter(find_samples)
        samples_in_clusters(cluster_id, result, target_names, 'K-means')
        
        
    # calculate other clustering metrics
        
    # Negative values mean independent labelings. Random labelings are close to 0.0. 1.0 stands for perfect match.
    adjusted_rand.append(metrics.adjusted_rand_score(y, cluster_labels))
    # A high value indicates a good similarity between two clusters
    fowlkes_mallows.append(metrics.fowlkes_mallows_score(y, cluster_labels))
    # High score for dense and well separated clusters
    calinski_harabaz.append(metrics.calinski_harabaz_score(X, cluster_labels)) 
               
    homogeneity.append(metrics.homogeneity_score(y, cluster_labels))
    completeness.append(metrics.completeness_score(y, cluster_labels))
    v_measure.append(metrics.v_measure_score(y, cluster_labels))
    
    plot_clustering_metrics(adjusted_rand, fowlkes_mallows, calinski_harabaz, ['Adjusted rand', 'Fowlkes mallows', 'Calinski harabaz'], 'K-means')
    plot_clustering_metrics(homogeneity, completeness, v_measure, ['Homogeneity', 'Completeness', 'V-Measure'], 'K-means')
    
    
    

def run_hierarchical(X, y, target_names):  
      
    # Clustering Metrics
    adjusted_rand = []
    fowlkes_mallows = []
    calinski_harabaz = []
    homogeneity = []
    completeness = []
    v_measure = []
        
    n_clusters = 3
        
    # Initialize the clusterer to implement Hierarchical algorithm

    linkage_types = ('ward', 'average', 'complete')
    for linkage in linkage_types: #best is ward
        clusterer = AgglomerativeClustering(n_clusters=n_clusters, linkage=linkage)
        clusterer.fit(X)
        cluster_labels = clusterer.fit_predict(X)
            
        # plot silhouette metric values
        plot_silhouette(X, n_clusters, cluster_labels, 'Hierarchical')
        plt.suptitle("Hierarchical (linkage="+str(linkage)+")")
            
            
        # plot clustering result
        for cluster_id in range(0,n_clusters):
            y = np.array(y)
            find_samples = y[cluster_labels == cluster_id]
            result = collections.Counter(find_samples)
            samples_in_clusters(cluster_id, result, target_names, 'Hierarchical')
            
        # calculate other clustering metrics
            
        # Negative values mean independent labelings. Random labelings are close to 0.0. 1.0 stands for perfect match.
        adjusted_rand.append(metrics.adjusted_rand_score(y, cluster_labels))
        # A high value indicates a good similarity between two clusters
        fowlkes_mallows.append(metrics.fowlkes_mallows_score(y, cluster_labels))
        # High score for dense and well separated clusters
        calinski_harabaz.append(metrics.calinski_harabaz_score(X, cluster_labels)) 
                   
        homogeneity.append(metrics.homogeneity_score(y, cluster_labels))
        completeness.append(metrics.completeness_score(y, cluster_labels))
        v_measure.append(metrics.v_measure_score(y, cluster_labels))
        
    plot_clustering_metrics(adjusted_rand, fowlkes_mallows, calinski_harabaz, ['Adjusted rand', 'Fowlkes mallows', 'Calinski harabaz'], 'Hierarchical', labels=linkage_types)
    plot_clustering_metrics(homogeneity, completeness, v_measure, ['Homogeneity', 'Completeness', 'V-Measure'], 'Hierarchical', labels=linkage_types)
    
    

    
def run_dbscan(X, y, target_names):   
    
    # Clustering Metrics
    adjusted_rand = []
    fowlkes_mallows = []
    calinski_harabaz = []
    homogeneity = []
    completeness = []
    v_measure = []
        
    n_clusters = 3
        
    # Initialize the clusterer to implement Kmeans algorithm
    clusterer = DBSCAN(eps=0.3, min_samples=2, metric='euclidean', algorithm='brute')
    cluster_labels = clusterer.fit_predict(X)          
                
    # plot silhouette metric values
    plot_silhouette(X, n_clusters, cluster_labels)
        
    # plot clustering result
    for cluster_id in range(0,n_clusters):
        y = np.array(y)
        find_samples = y[cluster_labels == cluster_id]
        result = collections.Counter(find_samples)
        samples_in_clusters(cluster_id, result, target_names, 'DBSCAN')
        
    # calculate other clustering metrics
        
    # Negative values mean independent labelings. Random labelings are close to 0.0. 1.0 stands for perfect match.
    adjusted_rand.append(metrics.adjusted_rand_score(y, cluster_labels))
    # A high value indicates a good similarity between two clusters
    fowlkes_mallows.append(metrics.fowlkes_mallows_score(y, cluster_labels))
    # High score for dense and well separated clusters
    calinski_harabaz.append(metrics.calinski_harabaz_score(X, cluster_labels)) 
               
    homogeneity.append(metrics.homogeneity_score(y, cluster_labels))
    completeness.append(metrics.completeness_score(y, cluster_labels))
    v_measure.append(metrics.v_measure_score(y, cluster_labels))
        
        
    plot_clustering_metrics(adjusted_rand, fowlkes_mallows, calinski_harabaz, ['Adjusted rand', 'Fowlkes mallows', 'Calinski harabaz'], "DBSCAN")
    plot_clustering_metrics(homogeneity, completeness, v_measure, ['Homogeneity', 'Completeness', 'V-Measure'], "DBSCAN")
    
    

def samples_in_clusters(cluster_id, result, target_names, algorithm):
    
    if not result:
        print ("empty cluster")
        return
    
    width = 0.2       # the width of the bars
    ind = [0]
    for i in range(1,len(result)):
        ind.append(ind[i-1]+width+0.1)  # the x locations for the groups
            
    plt.figure()
    axes = plt.gca()
        
    colors = ['darkcyan', 'greenyellow', 'red', 'hotpink', 'gold', 'darkmagenta']
        
    axes.bar(ind, result.values(), width, align='center', color=colors[:len(result)])
    axes.set_ylabel('Number of samples per category in the cluster')
    axes.set_title("Cluster "+str(cluster_id))
    axes.set_xticks(ind)
    labels = [target_names[i] for i in result.keys()]  #x axis labels are the categories into which the samples were classified
    axes.set_xticklabels(labels)
    axes.set_xlim([-0.6,ind[-1]+0.6])
    axes.set_ylim([0,max(result.values())+1])
    
    plt.savefig('figures\\clustering_results\\'+algorithm+ "\\" + str(layer) + '_cluster '+str(cluster_id))   # save the figure to file
    plt.close()
    
      
    
        
def plot_clustering_metrics(metric_1, metric_2, metric_3, metric_names, algorithm, labels=''):   
    # Create clustering metrics plot     
    width = 0.2     # the width of the bars
    ind = [0]       
    for i in range(1,len(metric_1)):
        ind.append(ind[i-1]+width+0.1)  # the x locations for the groups
            
    fig = plt.figure()
    colors = ['crimson', 'burlywood', 'darkgreen']   
    
    fig.suptitle(algorithm, fontsize=14)
    
    ax1 = fig.add_subplot(221)
    ax1.bar(ind, metric_1, width, align='center', color=colors[:len(metric_1)])
    
    ax2 = fig.add_subplot(222)
    ax2.bar(ind, metric_2, width, align='center', color=colors[:len(metric_2)])
    
    ax3 = fig.add_subplot(223)
    ax3.bar(ind, metric_3, width, align='center', color=colors[:len(metric_3)])
        
    ax1.set_ylabel('Scores')
    ax1.set_title(metric_names[0])
    ax1.set_xticks(ind)
    ax1.set_xticklabels(labels, fontsize=7)
    ax1.set_xlim([ind[0]-0.6,ind[-1]+0.6])
    
    ax2.set_ylabel('Scores')
    ax2.set_title(metric_names[1])
    ax2.set_xticks(ind)
    ax2.set_xticklabels(labels, fontsize=7)
    ax2.set_xlim([ind[0]-0.6,ind[-1]+0.6])
    
    ax3.set_ylabel('Scores')
    ax3.set_title(metric_names[2])
    ax3.set_xticks(ind)
    ax3.set_xticklabels(labels, fontsize=7)
    ax3.set_xlim([ind[0]-0.6,ind[-1]+0.6])
    
    plt.savefig('figures\\clustering_results\\'+algorithm+ "\\" + str(layer) + '_metrics_'+metric_names[0][0]+metric_names[1][0]+metric_names[2][0])   # save the figure to file
    plt.close()
    

def plot_silhouette(X, n_clusters, cluster_labels, algorithm):
    
    plt.figure()
    axes = plt.gca()
    axes.set_xlim([-1, 1])
    axes.set_ylim([0, len(X) + (n_clusters + 1) * 10])
        
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed clusters
    silhouette_avg = metrics.silhouette_score(X, cluster_labels)
        
    print("For n_clusters =", n_clusters,
            "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
    sample_silhouette_values = metrics.silhouette_samples(X, cluster_labels)
    
    y_lower = 10
        
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]
    
        ith_cluster_silhouette_values.sort()
    
        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i
    
        color = plt.get_cmap('Spectral')(float(i) / n_clusters)
        axes.fill_betweenx(np.arange(y_lower, y_upper),
                            0, ith_cluster_silhouette_values,
                            facecolor=color, edgecolor=color, alpha=0.7)
            
        # Label the silhouette plots with their cluster numbers at the middle
        axes.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples
    
    plt.title("Silhouette plot")
    plt.xlabel("The silhouette coefficient values")
    plt.ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
    axes.axvline(x=silhouette_avg, color="red", linestyle="--")
    
    axes.set_yticks([])  # Clear the yaxis labels / ticks
    axes.set_xticks([-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1]) 

    plt.savefig('figures\\clustering_results\\'+algorithm+ "\\" + str(layer) + '_silhouette')   # save the figure to file
    plt.close()


      
def cluster(X, y, target_names, layer):
        
    # AUTOMATED PREPROCESSING    
    
    # fill in missing values
    imp = preprocessing.Imputer(missing_values='NaN', strategy='mean', axis=0)
    imp.fit(X)
    X = imp.transform(X)
    
    # reject outliers
    clf = OneClassSVM(nu=0.1)
    clf.fit(X)
    outlier_pred = clf.predict(X) 
    inliers = [np.array(outlier_pred)==1]       
    X = X[inliers]  
    y = y[inliers]
    print (X.shape)
    
    # Create folder to store collected data if it does not already exist
    if not os.path.exists(os.getcwd()+"\\models"):
        os.makedirs(os.getcwd()+"\\models")   #current working directory + folder to create
    
    # scale features using statistics
    scaler = preprocessing.StandardScaler() #robust_scaler = preprocessing.RobustScaler()    
    X = scaler.fit_transform(X)
    
    # normalize data by scaling individual samples to have unit norm
    normalizer = preprocessing.Normalizer()
    X = normalizer.fit_transform(X)
    
    # Randomize data order
    random_data = list(zip(X, y))
    random.shuffle(random_data)
    X, y = zip(*random_data)
    X = np.array(X)
    y = np.array(y)
    
    # Create the RFE object and compute a cross-validated score.
    svc = SVC(kernel="linear")
    # The "accuracy" scoring is proportional to the number of correct
    # classifications
    rfecv = RFECV(estimator=svc, step=1, cv=StratifiedKFold(10), scoring='accuracy')
    rfecv.fit(X, y)
    
    print("Optimal number of features : %d" % rfecv.n_features_)
    print("Select features : " + str(rfecv.support_))
    
    X =  X[:, rfecv.support_]
    
        
    
    # CLUSTERING

    # Create folder to store collected data if it does not already exist
    #if not os.path.exists(os.getcwd()+"\\figures\\clustering_results\\K-means"):
    #    os.makedirs(os.getcwd()+"\\figures\\clustering_results\\K-means")
    #run_kmeans(X, y, target_names)
    
    
    if not os.path.exists(os.getcwd()+"\\figures\\clustering_results\\Hierarchical"):
        os.makedirs(os.getcwd()+"\\figures\\clustering_results\\Hierarchical")
    run_hierarchical(X, y, target_names)
    
    
    #if not os.path.exists(os.getcwd()+"\\figures\\clustering_results\\DBSCAN"):
    #    os.makedirs(os.getcwd()+"\\figures\\clustering_results\\DBSCAN")
    #run_dbscan(X, y, target_names)
       
    return X, y, rfecv.support_ # return to save in file     
 

   
# MAIN CLUSTERING CODE 
       
with open('system_data\\webpage_info.json', 'r') as f:
    webpage_info = json.load(f)
    
start_urls = webpage_info["webpages"].keys()
target_names = webpage_info["target_names"]
max_layer = webpage_info["max_layer"]

data = {}
for i in range(0, max_layer):
    layer = "layer_%02d" % 10
    print (layer)
    X, y = auxiliary_functions.create_data_array(start_urls, layer, 'data\\', target_names)
    
    # CLUSTERING
    X_pr, y_pr, features = cluster(X, y, target_names, layer)
    data[layer] = {}
    data[layer]["X"] = X_pr.tolist()
    data[layer]["y"] = y_pr.tolist()
    data[layer]["selected features"] = features.tolist()
 
# Store dataset to use in classification   
json_results = json.dumps(data, indent = 4)        
with open('system_data\\dataset.json', 'w') as f:
    f.write(json_results)


