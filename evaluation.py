from sklearn.externals import joblib
import os
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
import numpy as np
import auxiliary_functions
import json
from sklearn import preprocessing

from metric_calculator import calc_metrics
from data_collector import collect_data
from data_analysis import manual_preprocess



def evaluate_design(urls):
    
    # Load models created for prediction (models are located in the file 'models' of the current workspace)
    clf1 = joblib.load('models\\layer_00_KNNclf3.pkl') 
    clf2 = joblib.load('models\\layer_07_DTclf.pkl') 
    clf3 = joblib.load('models\\layer_09_DTclf.pkl') 
    clf4 = joblib.load('models\\layer_10_KNNclf4.pkl') 
    clf5 = joblib.load('models\\layer_12_KNNclf2.pkl')
    
    # Preprocess evaluation data in the same way as the training data
    evaluation_data = {}
    
    for i in [0, 7, 9, 10, 12]:
        layer = "layer_%02d" % i
        selected_features = dataset[layer]["selected features"]
        X_eval, y_eval = auxiliary_functions.create_data_array(urls, layer, 'eval_data\\', target_names)
       
        # fill in missing values
        imp = preprocessing.Imputer(missing_values='NaN', strategy='mean', axis=0)
        imp.fit(X_eval)
        X_eval = imp.transform(X_eval)
        
        # Scale and normalize data with the same models as the training data
        scaler = preprocessing.StandardScaler()
        X_eval = scaler.fit_transform(X_eval)
        normalizer = preprocessing.Normalizer()
        X_eval = normalizer.fit_transform(X_eval)
        
        # Keep only the selected features in this layer
        X_eval =  X_eval[:, selected_features]
        evaluation_data[layer] = {}
        evaluation_data[layer]['X'] = X_eval
        
    
    # Evaluate models on given data
    y_pred1 = clf1.predict_proba(evaluation_data['layer_00']['X'])    #probability of assignment to each class
    y_pred2 = clf2.predict_proba(evaluation_data['layer_07']['X'])
    y_pred3 = clf3.predict_proba(evaluation_data['layer_09']['X'])
    y_pred4 = clf4.predict_proba(evaluation_data['layer_10']['X'])
    y_pred5 = clf5.predict_proba(evaluation_data['layer_12']['X'])
    
    
    print np.array(y_pred1)
    print np.array(y_pred2)
    print np.array(y_pred3)
    print np.array(y_pred4)
    print np.array(y_pred5)
    
    
    # Aggregate results
    
    #class weights for each classifier
    w = np.array([[0.6,0.8,0.7],[0.9,0.6,0.9],[0.8,0.8,1],[1,1,1],[1,0.8,0.6]])     
    
    # multiply results with given weights
    res1 = np.multiply(y_pred1, w[0,:])
    res2 = np.multiply(y_pred2, w[1,:])
    res3 = np.multiply(y_pred3, w[2,:])
    res4 = np.multiply(y_pred4, w[3,:])
    res5 = np.multiply(y_pred5, w[4,:])
    
    # find mean value of results per class/category for each webpage
    result = np.sum([res1, res2, res3, res4, res5], axis=0)
    result = np.divide(result,5)
    
    # find category with maximum probability
    y_pred = np.argmax(result, axis=1)
    print y_pred
    print y_eval
    
      
    conf_mat = confusion_matrix(y_eval, y_pred)
    accuracy = []
    for i, result in enumerate(conf_mat):
        accuracy.append(float(result[i])/sum(result))
    
    report = classification_report(y_eval, y_pred, target_names=target_names)  
    print (report)
    print ("--ACCURACY--")
    for i, target in enumerate(target_names):
        print (target + " : " + str(accuracy[i]))
    print ("Average accuracy : " + str(accuracy_score(y_eval, y_pred)))
    

# MAIN EVALUATION CODE 
    
evaluation_urls = []

news = ['http://www.usatoday.com/',  'http://www.independent.ie/',
        'http://www1.cbn.com/home', 'http://news.gc.ca/web/index-en.do',
        'https://my.yahoo.com/', 'https://www.alarabiya.net/']

shopping = ['https://www.walgreens.com/?experience=B', 'http://www.barnesandnoble.com/',
            'https://www.wayfair.com/', 'https://www.humblebundle.com/star-wars-3-bundle',
            'http://eu.wiley.com/WileyCDA/Section/index.html', 'http://www.staples.com/office/supplies/home']

search_engines = ['http://isearch.babylon.com/', 'http://www.peekyou.com/',
                  'http://www.kiddle.co/', 'https://www.deepdyve.com/', 'http://hotbot.com/', 
                  'http://pdfsb.net/']

print len(news)
print len(shopping)
print len(search_engines)

evaluation_urls.extend(news)
evaluation_urls.extend(shopping)
evaluation_urls.extend(search_engines)
    
categories = ["news"]*len(news)
categories.extend(["e-shopping"]*len(shopping))
categories.extend(["search engine"]*len(search_engines))
target_names = (np.unique(categories)).tolist()   # sorted names of the categories used in the analysis

metrics = ['density', 'equilibrium', 'balance', 'regularity', 'simplicity', 'homogeneity', 'alignment', 'proportion', 
               'cohesion', 'grouping', 'symmetry', 'rhythm']

# COLLECT DATA

eval_webpage_info  = {}
eval_webpage_info["target_names"] = target_names
eval_webpage_info["webpages"] = {}

# Create folder to store collected data if it does not already exist
if not os.path.exists(os.getcwd()+"\\eval_data"):
    os.makedirs(os.getcwd()+"\\eval_data")   #current working directory + folder to create
 eval_webpage_info =  collect_data(evaluation_urls, categories, eval_webpage_info, 'eval_data\\')
json_results = json.dumps(eval_webpage_info, indent = 4)        
with open('eval_data\\eval_webpage_info.json', 'w') as f:
    f.write(json_results)

# Load webpage info
with open('eval_data\\eval_webpage_info.json', 'r') as f:
    eval_webpage_info = json.load(f)
 
# CALCULATE METRICS
calc_metrics(eval_webpage_info, 'eval_data\\', metrics)    #give file path to read data and write results

# PERMORM INITIAL PREPROCESSING
max_layer, metric_names = manual_preprocess(evaluation_urls, 'eval_data\\')   #get evaluation data

eval_webpage_info["max_layer"] = max_layer
eval_webpage_info["metric_names_ordered"] = metric_names
print metric_names


# Load system data to find selected features for each layer
with open('system_data\\dataset.json', 'r') as f:
    dataset = json.load(f)


# PREDICT WEBPAGE CATEGORY
evaluate_design(evaluation_urls)
        