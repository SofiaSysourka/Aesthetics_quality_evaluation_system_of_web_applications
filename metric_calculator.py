import re
import json
import Metrics



def calc_metrics(webpage_info, path, metrics):
    for url in webpage_info["webpages"].keys():
        
        category = webpage_info["webpages"][url]["category"]
        
        metricResults = []
        metricResults.append(Metrics.density(url, path))
        metricResults.append(Metrics.equilibrium(url, path))
        metricResults.append(Metrics.balance(url, path))
        metricResults.append(Metrics.regularity(url, path))
        metricResults.append(Metrics.simplicity(url, path))
        metricResults.append(Metrics.homogeneity(url, path))
        metricResults.append(Metrics.alignment(url, path))
        metricResults.append(Metrics.proportion(url, path))
        metricResults.append(Metrics.cohesion(url, path))
        metricResults.append(Metrics.grouping(url, path))
        metricResults.append(Metrics.symmetry(url, path))
        metricResults.append(Metrics.rhythm(url, path))
           
    
        #Reading data 
        filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
        filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
        with open(path + filename + '_data.json', 'r') as f:
            data = json.load(f)
            
        num_of_layers = data["scrapingResults"]["numOfLayers"]
    
          
        data = {}
        data['results'] = {}
        data['results'][url] = {}
        data['results'][url]['numOfLayers'] = num_of_layers
        data['results'][url]['category'] = category
        data['results'][url]['data'] = {}
        
        # Create json file with the metric values of each url
        for layer in range(0,num_of_layers):
            data['results'][url]['data']["layer_%02d" % layer] = {}
            for i,metric in enumerate(metrics):
                data['results'][url]['data']["layer_%02d" % layer][metric] = metricResults[i][layer]
        json_results = json.dumps(data, indent = 4)
        
        with open(path + filename + '_results.json', 'w') as log_file:
            log_file.write(json_results)


# MAIN METRIC CALCULATOR CODE
if __name__ == '__main__':
    metrics = ['density', 'equilibrium', 'balance', 'regularity', 'simplicity', 'homogeneity', 'alignment', 'proportion', 
               'cohesion', 'grouping', 'symmetry', 'rhythm']
    
    with open('system_data\\webpage_info.json', 'r') as f:
        webpage_info = json.load(f)
        
    calc_metrics(webpage_info, 'data\\', metrics)    #give file path to read data and write results


