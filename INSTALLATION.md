In order for the code to run, you need to install the following tools:

1. [Download](https://www.python.org/downloads/) and install Python
2. Install casperjs using the [instructions](http://docs.casperjs.org/en/latest/installation.html)
3. Install scikit-learn using the [instructions](http://scikit-learn.org/stable/install.html)
