import matplotlib.pyplot as plt
import numpy as np
import re
import json
import collections
from itertools import islice
from numbers import Number
from scipy import stats
import os

import auxiliary_functions



# Preprocess data to discard unnecessary layers or metrics
def manual_preprocess(urls, path):
    max_layer = 0
    
    #Reading data
    for url in urls:
        filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
        filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
        with open(path+filename + '_results.json', 'r') as f:
            data = json.load(f)
            
            
        # Delete equilibrium
        for layer in data["results"][url]["data"]:
            del data["results"][url]["data"][layer]["equilibrium"]
         
        # Delete unnecessary layers in the beginning with negative or zero density
        # (happens mainly in the first layers because of element overlapping or document completely covered with elements)   
        data["results"][url]["data"] = collections.OrderedDict(sorted(data["results"][url]["data"].items()))    # sort layers in ascending order
        delete_layers = []
        
        for layer in data["results"][url]["data"]:
            if data["results"][url]["data"][layer]["density"] <= 0:
                delete_layers.append(layer)     # delete unnecessary layer in the beginning
            else:
                break
        
        for layer in delete_layers:
            del data["results"][url]["data"][layer]
            
        # if a metric is >1, round it to 1       
        for layer in data["results"][url]["data"]:
            for metric in data["results"][url]["data"][layer]:
                if data["results"][url]["data"][layer][metric] > 1:
                    data["results"][url]["data"][layer][metric] = 1
          
          
        # Delete consecutive layers with the same metric values 
        data["results"][url]["data"] = collections.OrderedDict(sorted(data["results"][url]["data"].items()))    # sort layers in ascending order
        compare_layer = data["results"][url]["data"].keys()[0]          #get first layer
        
        for layer in islice(data["results"][url]["data"].keys(), 1, None): #skip first layer and check the rest
            flag = True
            # if all metric values are the same as those in the previous layer, delete the current layer
            for metric in data["results"][url]["data"][layer]:
                if data["results"][url]["data"][layer][metric] != data["results"][url]["data"][compare_layer][metric]:
                    flag = False
                    break
            if flag:
                del data["results"][url]["data"][layer]
            else:
                compare_layer = layer   #the current layer will be compared with the next one
          
        # Delete last layers where the metric values are almost the same
        data["results"][url]["data"] = collections.OrderedDict(sorted(data["results"][url]["data"].items(), reverse=True))
        compare_layer = data["results"][url]["data"].keys()[0]   #get last layer
        
        for layer in islice(data["results"][url]["data"].keys(), 1, None):
            flag = True
            # if all metric values are similar to those in the previous layer, delete the previous layer
            for metric in data["results"][url]["data"][layer]:
                if not(isinstance(data["results"][url]["data"][layer][metric], Number) and isinstance(data["results"][url]["data"][compare_layer][metric],Number)):
                    continue
                if abs(data["results"][url]["data"][layer][metric] - data["results"][url]["data"][compare_layer][metric])>0.05:
                    flag = False    #not all metrics have similar values
                    break
            if flag:
                del data["results"][url]["data"][compare_layer]
                compare_layer = layer
            else:
                break
        
        # Rename layers
        data["results"][url]["data"] = collections.OrderedDict(sorted(data["results"][url]["data"].items()))
        new_data = {}
        new_data['results'] = {}
        new_data['results'][url] = {}
        new_data['results'][url]['category'] = data['results'][url]['category']
        new_data['results'][url]['data'] = {}

        for i, layer in enumerate(data["results"][url]["data"]):
            new_data['results'][url]['data']["layer_%02d" % i] = {}
            new_data['results'][url]['data']["layer_%02d" % i] = data['results'][url]['data'][layer]
        new_data["results"][url]["data"] = collections.OrderedDict(sorted(new_data["results"][url]["data"].items()))
        
        # Write new data to file
        json_results = json.dumps(new_data, indent = 4)        
        with open(path + filename + '_results_processed.json', 'w') as log_file:
            log_file.write(json_results) 
        
        # Find maximum number of layers in all the web pages
        if len(new_data["results"][url]["data"]) > max_layer:
            max_layer = len(new_data["results"][url]["data"])  
              
    # Repeat last layer to reach max number of layers    
    for url in urls:
        filename = re.split(re.compile(".*?//(www)?(.*)\.",re.DOTALL ),url)[2]
        filename = re.sub(re.compile("^\.",re.DOTALL),"",filename)
        with open(path + filename + '_results_processed.json', 'r') as f:
            data = json.load(f)
            
        data["results"][url]["data"] = collections.OrderedDict(sorted(data["results"][url]["data"].items()))
        num_of_layers = len(data["results"][url]["data"])
        last_layer = data["results"][url]["data"].keys()[-1]   #get last layer

        for i in range(num_of_layers, max_layer):
            data["results"][url]["data"]["layer_%02d" % i] = {}
            data["results"][url]["data"]["layer_%02d" % i] = data["results"][url]["data"][last_layer]   #repeat last layer
        
        json_results = json.dumps(data, indent = 4)        
        with open(path + filename + '_results_processed.json', 'w') as log_file:
            log_file.write(json_results)
    
    # Find the way metrics are ordered for a random url because they are ordered in the same way for all urls
    metrics_ordered = data["results"][url]["data"]["layer_00"].keys()   
    
    return max_layer, metrics_ordered
 
 
def category_corr(start_urls, categories, target_names, max_layer, metric_names):   
    # create data array for each category
    data = {}
        
    for target in target_names:
        data[target] = {}
        print target
        select = np.array(start_urls)[np.array(categories) == target]   #select minimum number of samples from current target category
        #select = select[:min_num_samples]
        
        # create data array of current category samples for all layers
        for idx, sample in enumerate(select): 
            X = np.empty(shape=(0,len(metric_names)))
            for layer in range(0, max_layer):
                temp, _ = auxiliary_functions.create_data_array([sample], "layer_%02d" % layer, target_names)
                X = np.concatenate((X,temp))                
            data[target]["sample_%02d" % idx] = X   
        print X.shape
        
    for idx, category1 in enumerate(data):                  
        for category2 in data.keys()[(idx+1):]:
            # calculate correlation between the samples of the categories            
            category_corr_utility(data[category1], data[category2], [category1, category2], metric_names)


def category_corr_utility(category1, category2, category_names, metric_names):
    total_corr = np.zeros(len(metric_names))
    n1_samples = len(category1)
    n2_samples = len(category2)
        
    for ith_sample in category1: 
        test1 = category1[ith_sample]
        
        for jth_sample in category2:
            test2 = category2[jth_sample]
            
            # Correlation between each metric for two different web page categories or for two different layers
            metric_corr = []
            for metric1, metric2 in zip(test1.T, test2.T):
                metric_corr.append(stats.pearsonr(metric1, metric2))    #tuple: (Pearson correlation coefficient, p-value non-correlation testing)
        
            metric_corr = [i[0] for i in metric_corr]   #keep only the correlation coefficients
            total_corr = np.sum([total_corr, np.array(metric_corr)], axis=0)
            
    total_corr = total_corr/(n1_samples*n2_samples)     # mean correlation between metrics
            
    # plot correlation result
    width = 0.2       # the width of the bars
    ind = [0]
    for i in range(1,len(metric_corr)):
        ind.append(ind[i-1]+width+0.1)  # the x locations for the groups
                        
    plt.figure()
    axes = plt.gca()
    axes.bar(ind, metric_corr, width, align='center', color='darkcyan')
    axes.set_ylabel('Correlation coefficients')
    axes.set_title('Metric correlation')
    plt.suptitle(category_names[0]+' - '+category_names[1])
    axes.set_xticks(ind)
    axes.set_xticklabels(metric_names, rotation=45)
    axes.set_xlim([-0.6,ind[-1]+0.6])
    axes.set_ylim([-1,1])
    plt.show()
        
      
  
               
def metric_value_range(start_urls, metric_names, max_layer):
    
    for metric_name in metric_names:
        # Create folder to store collected data if it does not already exist
        if not os.path.exists(os.getcwd()+"\\figures\\metric_value_range\\"+metric_name):
            os.makedirs(os.getcwd()+"\\figures\\metric_value_range\\"+metric_name)   #current working directory + folder to create
    
    for layer in range(0, max_layer):
        X, _ = auxiliary_functions.create_data_array(start_urls, "layer_%02d" % layer, 'data\\')
        for idx, metric_data in enumerate(X.T):
            plt.hist(metric_data, bins=20)
            plt.title("Layer " + str(layer) + " - " + metric_names[idx])
            plt.savefig('figures\\metric_value_range\\'+metric_names[idx]+"\\Layer " + str(layer) + " - " + metric_names[idx])   # save the figure to file
            plt.close()
          

def metric_corr_full_dataset(start_urls, metric_names, max_layer):
    
    X = np.empty(shape=(0,len(metric_names)))
    for layer in range(0,max_layer):
        curr_layer = "layer_%02d" % layer
        temp, _ = auxiliary_functions.create_data_array(start_urls, curr_layer, 'data\\')
        X = np.concatenate((X,temp))
        
    # Correlation between metrics
    plt.imshow(np.corrcoef(X, rowvar=0), interpolation='nearest')
    axes = plt.gca()
    axes.grid(False)
    plt.xticks(np.arange(len(metric_names)), metric_names, rotation=45)
    plt.yticks(np.arange(len(metric_names)), metric_names)
    plt.colorbar()
    plt.show()
            

            
def metric_corr_between_layers(start_urls, metric_names, max_layer):
    
    data = {}
    for layer in range(0,max_layer):
        curr_layer = "layer_%02d" % layer
        X, _ = auxiliary_functions.create_data_array(start_urls, curr_layer, 'data\\')
        data[curr_layer] = X
    
    # correlate each layer with its following layer
    for layer in range(0,max_layer-1):
        metric_corr = [] 
        data1 = data["layer_%02d" % layer]
        data2 = data["layer_%02d" % (layer+1)]
        
        for metric_data1, metric_data2 in zip(data1.T, data2.T):
            metric_corr.append(stats.pearsonr(metric_data1, metric_data2))  #tuple
            
        metric_corr = [i[0] for i in metric_corr]   #keep only the correlation coefficients
            
        width = 0.2       # the width of the bars
        ind = [0]
        for i in range(1,len(metric_corr)):
            ind.append(ind[i-1]+width+0.1)  # the x locations for the groups
            
        plt.figure()
        axes = plt.gca()
        
        axes.bar(ind, metric_corr, width, align='center', color='darkcyan')
        axes.set_ylabel('Correlation coefficients')
        axes.set_title('Metric correlation between Layer '+str(layer)+' and Layer '+str(layer+1))
        axes.set_xticks(ind)
        axes.set_xticklabels(metric_names, rotation=45)
        #axes.set_xlim([-0.6,ind[-1]+0.6])
        axes.set_ylim([0,1])
        axes.axhline(y=0, color='k')
        plt.show()           
                




# STATISTICAL ANALYSIS
if __name__ == '__main__':    
    
    # Load data from json file
    with open('system_data\\webpage_info.json', 'r') as f:
        webpage_info = json.load(f)
    
    start_urls = webpage_info["webpages"].keys()
    target_names = webpage_info["target_names"]
    categories = [webpage_info["webpages"][url]["category"] for url in webpage_info["webpages"].keys()]
    
    # Perform initial preprocessing
    # and find maximum number of layers after preprocessing, and the order of the metrics in the saved file
    max_layer, metric_names = manual_preprocess(start_urls, 'data\\')   #get training data from data file
    
    webpage_info["max_layer"] = max_layer
    webpage_info["metric_names_ordered"] = metric_names
    
    json_results = json.dumps(webpage_info, indent = 4)        
    with open('system_data\\webpage_info.json', 'w') as f:
        f.write(json_results)
    
    
    # Correlation between the metrics of the different categories
    #category_corr(start_urls, categories, target_names, max_layer, metric_names)
    
    # Correlation between the metrics of the whole dataset
    metric_corr_full_dataset(start_urls, metric_names, max_layer)
    
    # Correlation between the metrics of the different layers
    #metric_corr_between_layers(start_urls, metric_names, max_layer)
    
    # Range of metric values in all web pages (general design patterns)
    #metric_value_range(start_urls, metric_names, max_layer)

