## Project name
Design and development of an aesthetics quality evaluation system of web applications based on structural analysis

## Synopsis
This project is about the design and development of a system which evaluates the user interfaces of webpages based on user-perceived aesthetics through structural 
analysis. Τhe goal is to determine the dominant design patterns of the user interfaces of popular webpages, as well as the patterns which differentiate the websites based on 
their content.

## Installation
Read about the installation steps needed for the project to work in [INSTALLATION.md](https://gitlab.com/SofiaSysourka/Aesthetics_quality_evaluation_system_of_web_applications/blob/master/INSTALLATION.md)

## Usage
Learn about how the code is used in [USAGE.md](https://gitlab.com/SofiaSysourka/Aesthetics_quality_evaluation_system_of_web_applications/blob/master/USAGE.md)